
var app = angular.module('market', []);

// loginCtrl handles the login process
app.controller('loginCtrl', function($scope, $http, $httpParamSerializerJQLike, $window) {

    $scope.username = "";
    $scope.password = "";
    $scope.login = function() {
        console.log("data: ", $httpParamSerializerJQLike({
            "username":$scope.username,
            "password":$scope.password
        }));

        $http({
            method  : 'POST',
            url     : '/api/authenticate',
            data    : $httpParamSerializerJQLike({
                "username":$scope.username,
                "password":$scope.password
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
        })
        .then(
            function successCallback(data) {
                // TODO: need this location change to be role-based, or perhaps based on last view
                $window.location.href = '/grower-offers.html';
            },
            function failureCallback(data) {
                // data.data has error detail if interested
                $(".w-form-fail").show();
            }
        );        
    }
});
var app = angular.module('market', ['sprintf', 'ngFileUpload']);

// offerCtrl handles the creation and editing of offers
app.controller('offerCtrl', ['$scope', 'Upload', '$http', '$window', '$location', '$timeout', function($scope, Upload, $http, $window, $location, $timeout) {

    $scope.priceBreaks = [
        {
            "quantity": null,
            "netPrice": null,
            "grossPrice": null
        }
    ];

    $scope.uploadFiles = function(file, errFiles) {
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: '/v1/item-images',
                data: {file: file}
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    console.log("Upload complete:", file.result.imageUUID);
                    $scope.offer.imageUUIDs[0] = file.result.imageUUID;
                });
            }, function (response) {
                if (response.status > 0) {
                    $scope.errorMsg = response.status + ': ' + response.data;
                }
                console.log("error: ", response);
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    }   

    $scope.loadOffer = function (id) {

        $http({
            method  : 'GET',
            url     : '/v1/offers/' + id,
        })
        .then(
            function successCallback(data) {
                console.log("success", data);
                $scope.offer = data.data;
                $scope.initObject();
            },
            function failureCallback(data) {
                console.log("error", data);
                // data.data has error detail if interested
                $(".w-form-fail").show();
            }
        );          
    };

    $scope.initObject = function() {
        var offer = $scope.offer;
        $scope.f = {'valid': true};
        $scope.initPriceBreakData();
        if (offer.attributes.indexOf("organic") > -1) {
            $scope.attributeOrganic = true;
        }
        $scope.picFile = "/v1/offers/" + offer.offerID + "/images/" + offer.imageUUIDs[0];

        if (offer.deliveryDays.indexOf("monday") > -1) $scope.deliveryDayMonday = true;        
        if (offer.deliveryDays.indexOf("tuesday") > -1) $scope.deliveryDayTuesday = true;        
        if (offer.deliveryDays.indexOf("wednesday") > -1) $scope.deliveryDayWednesday = true;        
        if (offer.deliveryDays.indexOf("thursday") > -1) $scope.deliveryDayThursday = true;        
        if (offer.deliveryDays.indexOf("friday") > -1) $scope.deliveryDayFriday = true;        
        if (offer.deliveryDays.indexOf("saturday") > -1) $scope.deliveryDaySaturday = true;        
        if (offer.deliveryDays.indexOf("sunday") > -1) $scope.deliveryDaySunday = true;
    };

    $scope.createObject = function() {
        $scope.offer.status = "active";
        if ($scope.attributeOrganic) {
            $scope.offer.attributes = ["organic"];
        } else {
            $scope.offer.attributes = null;
        }
        $scope.offer.deliveryDays = [];
        if ($scope.deliveryDayMonday) $scope.offer.deliveryDays.push("monday");
        if ($scope.deliveryDayTuesday) $scope.offer.deliveryDays.push("tuesday");
        if ($scope.deliveryDayWednesday) $scope.offer.deliveryDays.push("wednesday");
        if ($scope.deliveryDayThursday) $scope.offer.deliveryDays.push("thursday");
        if ($scope.deliveryDayFriday) $scope.offer.deliveryDays.push("friday");
        if ($scope.deliveryDaySaturday) $scope.offer.deliveryDays.push("saturday");
        if ($scope.deliveryDaySunday) $scope.offer.deliveryDays.push("sunday");

        $scope.normalizePriceBreakData();

        $http({
            method  : verb,
            url     : id === "" ? '/v1/offers' : "/v1/offers/" + id,
            data    : $scope.offer,
            headers: {
                'Content-Type': "application/json"
            },
        })
        .then(
            function successCallback(data) {
                console.log("success", data);
                // TODO: need this location change to be role-based, or perhaps based on last view
                $window.location.href = '/grower-offers.html';
            },
            function failureCallback(data) {
                console.log("error", data);
                // data.data has error detail if interested
                $(".w-form-fail").show();
            }
        );        

    }

    // Price Breaks Control

    var commissionRate = 0.2;

    $scope.removeSelectedPriceBreak = function() {
        var newDataList = [];
        $scope.selectedAll = false;
        angular.forEach($scope.priceBreaks, function(selected) {
            if (!selected.selected) {
                newDataList.push(selected);
            }
        });
        $scope.priceBreaks = newDataList;        
    };

    $scope.addNewPriceBreakRow = function() {
        $scope.priceBreaks.sort(comparePriceBreakData);
        var quantity = null;
        if ($scope.offer.willSupply) {
            quantity = $scope.offer.willSupply.quantity;
        }
        $scope.priceBreaks.push(
            {
                "quantity":  quantity,
                "netPrice": null,               // what the supplier gets
                "grossPrice": null              // what the buyer pays
            }
        );        
    };

    function comparePriceBreakData(a, b) {
        if (a.quantity < b.quantity) return -1;
        if (a.quantity > b.quantity) return 1;
        return 0;
    }


    $scope.initPriceBreakData = function() {
        $scope.priceBreaks = [];
        for (i = 0; i < $scope.offer.pricing.length; ++i) {
            var entry = $scope.offer.pricing[i];
            $scope.priceBreaks.push({
                'quantity': entry.max,
                'grossPrice': entry.price,
                'netPrice': null
            });
        }
    };

    $scope.normalizePriceBreakData = function() {
        $scope.offer.pricing = [];
        $scope.priceBreaks.sort(comparePriceBreakData);
        var minQuantity = 1;
        var maxQuantity;
        // parse out the junk
        for (i = 0; i < $scope.priceBreaks.length; ++i) {
            var entry = $scope.priceBreaks[i];
            if (entry.quantity === null || entry.quantity === 0.0) {
                $scope.priceBreaks.splice(i, 1);
            }
        }
        for (i = 0; i < $scope.priceBreaks.length; ++i) {
            var entry = $scope.priceBreaks[i];
            maxQuantity = entry.quantity;
            $scope.offer.pricing.push({
                "min": minQuantity, 
                "max": maxQuantity, 
                "price": entry.grossPrice
            });
            minQuantity = maxQuantity + 1;
        }        
    };

    function roundTo(n, digits) {
        if (digits === undefined) {
            digits = 0;
        }

        var multiplicator = Math.pow(10, digits);
        n = parseFloat((n * multiplicator).toFixed(11));
        var test = (Math.round(n) / multiplicator);
        return +(test.toFixed(digits));
    };

    $scope.changeNetPrice = function(value, index) {
        $scope.priceBreaks[index].grossPrice = roundTo(value * (commissionRate + 1),2);
    };

    $scope.changeGrossPrice = function(value, index) {
        $scope.priceBreaks[index].netPrice = roundTo(value / (commissionRate + 1),2)
    };


    // initialization

    var verb = "POST";
    params={};location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){params[k]=v})
    var id = params["id"];
    if (id === undefined) id = "";
    if (id !== "") {
        verb = "PUT";
        $scope.loadOffer(id);
        $("#submit-button").attr("VALUE", "UPDATE OFFER");
    } else {
        $scope.offer = {};
        $scope.offer.category = "";
        $scope.offer.imageUUIDs = [];
        $scope.attributeOrganic = false;
        $scope.deliveryDayMonday = false;
        $scope.deliveryDayTuesday = false;
        $scope.deliveryDayWednesday = false;
        $scope.deliveryDayThursday = false;
        $scope.deliveryDayFriday = false;
        $scope.deliveryDaySaturday = false;
        $scope.deliveryDaySunday = false;
    }
    

}]);

/*
{
    "offerID": "<uuid generated by the system>",
    "status": "active",
    "title": "Purple Carrots",
    "description": "These carrots are great.",
    "attributes": [
        "organic"
    ],
    "imageIDs": [
        "df8ec55b-abf8-412e-aa9c-df704d4175ff"
    ],
    "category": "001.006",
    "willSupply": {
        "quantity": 33.5,
        "uom": "pound",
        "firstDate": "2017-06-09",
        "lastDate": "2017-08-30"
    },
    "pricing": [
        {
            "min": 1,
            "max": 20,
            "price": 1.94
        },
        {
            "min": 21,
            "max": 40,
            "price": 1.78
        }
    ],
    "deliveryDays": [
        "tuesday",
        "friday"
    ],
    "notes": "here are some notes to the market manager"
}
*/
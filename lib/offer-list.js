var app = angular.module('market', ['sprintf', 'infinite-scroll']);

// offerListCtrl handles the displaying offers
app.controller('offerListCtrl', function($scope, $http, $window) {

    $scope.loadedRecordCount = 0;
    $scope.currentPage = 0;    
    $scope.pageSize = 10;
    $scope.offers = [];
    $scope.totalCount = 0;
    $scope.busy = false;

    $scope.GetCategoryLabel = function(offer) {
        return offer.category;
    };

    $scope.GetAttributesLabel = function(offer) {
        var label = ""
        if (offer.attributes !== undefined) {
            for (s of offer.attributes) {
                switch (s) {
                    case "organic":
                        label += "Certified Organic";
                        break;
                }
            }
        }
        return label;
    };

    $scope.GetOfferImageData = function(offer) {
        var image = offer.images[0];
        return "data:" + image.format + ";base64," + image.image;
    };

    $scope.GetPaginationLabel = function() {
        //console.log("paginationlabel requested", $scope.loadedRecordCount, $scope.totalCount)
        return sprintf("Showing %d of %d current %s", $scope.loadedRecordCount, $scope.totalCount, pluralize("offer", $scope.totalCount));
    };

    $scope.loadNextPage = function() {
        console.log("Calling loadnextpage");
        if ($scope.busy) return;
        $scope.busy = true;
        ++$scope.currentPage;
        var url = sprintf("/v1/offers?expand=true&limit=%d&page=%d", $scope.pageSize, $scope.currentPage);
        $http.get(url)
            .then(
            function successCallback(result) {
                var items = result.data;
                if (items.length > 0) {
                    for (var i = 0; i < items.length; i++) {
                        $scope.offers.push(items[i]);
                    }
                    $scope.totalCount = result.headers("X-Total-Count");
                    $scope.loadedRecordCount = $scope.offers.length;
                    console.log("records retrieved", $scope.loadedRecordCount, "of", $scope.totalCount);
                }
                $scope.busy = false;
            },
            function errorCallback(result) {
                if (result.status == 401 || result.status == 403) {
                    $window.location.href = '/index.html';
                }
            }
        );
    }
});

app.directive('backImg', function(){
    return function(scope, element, attrs){
        attrs.$observe('backImg', function(value) {
            var offer = JSON.parse(value);
            if (offer == null) return;
            var image = offer.images[0];
            element.css({
                'background-image': "url(" + image.imageURL + ")",
                'background-size' : 'cover'
            });
        });
    };
});

const walk = require("walk");
const cheerio = require('cheerio');
const fs = require("fs");

// Walker options
var filters = ["processed", "node_modules", ".git"]
var walker  = walk.walk('.', { followLinks: false, filters: filters });

const processed = "processed/";
var paths = ["components", "lib", "css", "images", "js"];

try {
    fs.lstatSync(processed).isDirectory()
} catch (e) {
    fs.mkdirSync(processed);    
}

var componentPath = processed + "components";
if (fs.existsSync(componentPath)) {
    fs.readdirSync(componentPath).forEach(function(file,index){
        var curPath = componentPath + "/" + file;
        if (fs.lstatSync(curPath).isDirectory()) {
            deleteFolderRecursive(curPath);
        } else {
            fs.unlinkSync(curPath);
        }
    });
    fs.rmdirSync(componentPath);
}

for (let path of paths) {
    try {
        fs.lstatSync(processed + path).isDirectory()
    } catch (e) {
        fs.mkdirSync(processed + path);    
    }
};

walker.on('file', function(root, stat, next) {
    var source = root + '/' + stat.name;
    // Add this file to the list of files
    if (stat.name.endsWith(".html")) {
        fs.readFile(source, (err, data) => {
            if (err) throw err;
            console.log("Processing " + source);
            const $ = cheerio.load(data);

            // Remove elements with mock and design-note classes
            $(".mock").remove();
            $(".design-note").remove();

            // Rename all w-form class assignments to nil-form
            $(".w-form").removeClass('w-form').addClass('nil-form');

            // Extract components into components folder and then remove them
            $(".mahina-component").each(function(index, element) {
                var id = $(element).attr("id");
                id = id.replace("mahina_component_", "") + "_component";
                $(element).attr("id", id);
                //console.log('looking at ', $(element).parent().html());
                fileName = id.replace(/_/g, "-") + ".html";
                if (!fs.existsSync(processed + "components/" + fileName)) {
                    fs.writeFile(processed + "components/" + fileName, $(element).parent().html(), (err) => {
                        if (err) throw err;
                        console.log('Created and wrote component ' + fileName + ' to ' + processed + "/components/");
                    });
                }
                repeatAttribute = $(element).parent().attr("ng-repeat");
                if (repeatAttribute !== undefined) {
                    $(element).parent().append("<div ng-repeat='" + repeatAttribute + "' ng-include=\"'components/" + fileName + "'\"></div>");
                    $(element).parent().removeAttr("ng-repeat");
                } else {
                    $(element).parent().append("<ng-include src=\"'components/" + fileName + "'\"></ng-include>");
                }
            });
            $(".mahina-component").remove();

            // Swap replaceable DIVs with source from /components folder
            $(".mahina-replace").each(function(index, element) {
                var sourceName = $(element).attr("mahina-source")
                if (sourceName == "") {
                    throw "No mahina-source attribute found in " + $(element).attr("id");
                }
                contents = fs.readFileSync("components/" + sourceName + ".html");
                $(element).replaceWith(contents.toString());
            });

            fs.writeFile(processed + source, $.html(), (err) => {
                if (err) throw err;
                console.log('Copied ' + source + ' to ' + processed);
            });            
        });
    } else if (stat.name == "webflow.js") {
        fs.readFile(source, (err, data) => {
            if (err) throw err;
            result = data.toString().replace("if (type === 'password') {", "if (type === 'passwordNOT') {")
            fs.writeFileSync(processed + source, result);
            console.log("Processed webflow.js to ", processed);
        });
    } else if (stat.name == ".DS_Store" || stat.name == "process.js") {
        console.log('Ignoring', stat.name);
    } else {
        fs.createReadStream(source).pipe(fs.createWriteStream(processed+source));
        console.log('Copied ' + source + ' to ' + processed);
    }
    next();
});




